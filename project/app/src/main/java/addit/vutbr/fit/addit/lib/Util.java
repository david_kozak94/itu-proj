package addit.vutbr.fit.addit.lib;

import java.util.Calendar;

/**
 * Created by david on 9.12.16.
 */

public class Util {

    public static java.sql.Date getCurrentDate(){
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();
        return new java.sql.Date(date.getTime());
    }
}
