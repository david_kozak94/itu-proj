package addit.vutbr.fit.addit.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import addit.vutbr.fit.addit.Controller;
import addit.vutbr.fit.addit.R;
import addit.vutbr.fit.addit.model.State;
import addit.vutbr.fit.addit.model.Task;
import addit.vutbr.fit.addit.view.AddNew;
import addit.vutbr.fit.addit.view.ListOfAll;
import addit.vutbr.fit.addit.view.MainPage;

import static addit.vutbr.fit.addit.view.MainPage.ADD_OR_EDIT;

/**
 * Adapter which connects data with the RecyclerView
 */
public class TaskByDateRecyclerViewAdapter extends RecyclerView.Adapter<TaskByDateRecyclerViewAdapter.TaskViewHolder> {
    /**
     * list of categories, each is characterized by task's dateCategory
     */
    private final List<DateCategory> categories = Arrays.asList(DateCategory.values());
    private final List<Task> allTasks;
    private final Activity context;

    private int counter = 0;

    public TaskByDateRecyclerViewAdapter(Activity context) {
        this.context = context;
        allTasks = new ArrayList<>(Controller.getInstance().getAllTasks(State.ACTIVE));
    }

    /**
     * called for each row, created the layout
     *
     * @param viewGroup
     * @param i
     * @return
     */
    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_one_row_recycler, null);
        DateCategory dateCategory = categories.get(counter++);
        if (counter == categories.size())
            counter = 0;
        return new TaskViewHolder(view, dateCategory);
    }

    /**
     * called when the given row is about to be displayed
     *
     * @param taskViewHolder
     * @param i
     */
    @Override
    public void onBindViewHolder(TaskViewHolder taskViewHolder, int i) {
        // get task on given index
        DateCategory state = categories.get(i);

        // set information from task to the layout
        taskViewHolder.categoryName.setText(state.toString());
        taskViewHolder.categoryName.setOnClickListener((v) -> {
            int visibility = taskViewHolder.categoryContent.getVisibility();
            if (visibility == View.VISIBLE)
                taskViewHolder.categoryContent.setVisibility(View.GONE);
            else
                taskViewHolder.categoryContent.setVisibility(View.VISIBLE);
        });
        taskViewHolder.reload();
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void reload() {
        allTasks.clear();
        allTasks.addAll(Controller.getInstance().getAllTasks(State.ACTIVE));
        notifyDataSetChanged();
    }

    /**
     * Each inflated layout has its holder
     * rows are reused, that is why it is neccasary to set corect values into the layout in onBindViewHolder
     * findViewByID is called only once per view, which saves time
     */
    class TaskViewHolder extends RecyclerView.ViewHolder {
        protected TextView categoryName;
        protected RecyclerView categoryContent;
        protected TaskRecyclerViewAdapter adapter;
        protected DateCategory dateCategory;

        public TaskViewHolder(View view, DateCategory dateCategory) {
            super(view);
            this.dateCategory = dateCategory;
            this.categoryName = (TextView) view.findViewById(R.id.recycler_task_category_name);
            this.categoryContent = (RecyclerView) view.findViewById(R.id.recycler_view_task_category_content);
            this.adapter = new TaskRecyclerViewAdapter(context, dateCategory.filterTasksForCategory(allTasks));
            this.categoryContent.setLayoutManager(new LinearLayoutManager(context));
            this.categoryContent.setAdapter(adapter);

            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    TaskRecyclerViewAdapter.TaskViewHolder taskViewHolder = (TaskRecyclerViewAdapter.TaskViewHolder) viewHolder;

                    new AlertDialog.Builder(context)
                            .setTitle(R.string.delete_task)
                            .setMessage(R.string.delete_task_confirm)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                                // delete the old and put in the new one
                                Controller controller = Controller.getInstance();
                                controller.markTaskAsDeleted(taskViewHolder.getTask());
                                Intent intent5 = new Intent(context, ListOfAll.class);
                                context.startActivity(intent5);
                                context.overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                            })
                            .setNegativeButton(android.R.string.no, (i,j) -> {
                                Intent intent5 = new Intent(context, ListOfAll.class);
                                context.startActivity(intent5);
                                context.overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                            }).show();
                }
            };

            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);

            itemTouchHelper.attachToRecyclerView(categoryContent);

        }

        public void reload() {
            this.adapter.setTasks(dateCategory.filterTasksForCategory(allTasks));
            this.adapter.notifyDataSetChanged();
        }
    }

    private enum DateCategory {
        Today,
        Tomorrow,
        Future;

        public List<Task> filterTasksForCategory(List<Task> allTasks) {
            Calendar c = new GregorianCalendar();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            Date today = c.getTime();
            c.add(Calendar.DATE, 1);
            Date tomorrow = c.getTime();
            c.add(Calendar.DATE, 1);
            Date afterTommorrow = c.getTime();
            List<Task> result = new ArrayList<>();
            for (Task task : allTasks) {
                Date taskDate = task.getDate();
                switch (this) {
                    case Today:
                        if (taskDate.compareTo(today) >= 0 && taskDate.compareTo(tomorrow) < 0) {
                            // bigger or equal today, lesser than tomorrow
                            result.add(task);
                        }

                        break;
                    case Tomorrow:
                        if (taskDate.compareTo(tomorrow) >= 0 && taskDate.compareTo(afterTommorrow) < 0) {
                            // bigger or equal tomorrow, lesser than after tomorrow
                            result.add(task);
                        }
                        break;
                    case Future:
                        if (taskDate.compareTo(afterTommorrow) > 0) {
                            // bigger than tomorrow
                            result.add(task);
                        }
                        break;
                    default:
                        throw new RuntimeException("Default in switch, should never happed");
                }
            }
            return result;
        }
    }
}